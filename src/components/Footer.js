import React from 'react'
import PropTypes from 'prop-types'

export default function Footer(props) {
  return (
    <div className={props.className}>
      a work in progress by JustHeath
    </div>
  )
}

Footer.propTypes = {
  className: PropTypes.string.isRequired,
}